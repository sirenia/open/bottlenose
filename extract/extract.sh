#!/usr/bin/env bash
for file in /input/*; do
  java -jar tika-app-1.27.jar --extract --extract-dir="/output" "$file"
done
